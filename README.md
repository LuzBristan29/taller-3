# practica

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

En este proyecto la función principal era corregir el parcial #2 haciendo uso
de diferentes comandos, como por ejemeplo el de mostrar y poder ejecutar la 
acción determinada.
Hacer uso de los componentes Enviar información a los componentes desde padre 
a hijos y de hijos a padres Hacer usos de las condiciones v-if y v-show